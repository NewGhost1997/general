<!-- Title: <group> <xy.z milestone> Planning issue -->

## Goal

1. goal 1
1. goal 2

## Quarterly OKR status

<!-- Summarize the status of OKRs here -->

## The Roadmap

The roadmap is maintained on [the Configure direction pages](https://about.gitlab.com/direction/configure/).

## Capacity planning

Update the with planned PTOs here - @nmezzopera

## The Board

As with any board in GitLab an issue might appear in multiple columns, we try to avoid this here. As a result originally UX issues might have lost the UX label already, if they are ready for dev work.
The Deliverable and Stretch columns are priority ordered. Let's hope/prey that the ordering won't change unexpectedly.
If you remove something from Deliverable or Stretch, or move an issue between these, please mention in here in a comment and at least mention @nagyv-gitlab to know about it

[Planning board](https://gitlab.com/groups/gitlab-org/-/boards/2395842) 

## Actions

Close issue if all these are done:

* [ ] - once the team is aligned, set ~"workflow::ready for development" on every issue in ~Deliverable and ~Stretch
* [ ] - create the kickoff video with the title `GitLab XX.XX Kickoff - Configure:Configure`
* [ ] - if there is [a kickoff issue](https://gitlab.com/gitlab-com/Product/issues?scope=all&utf8=%E2%9C%93&state=opened&search=kickoff), check it

/label ~"Planning Issue" ~"section::ops"
